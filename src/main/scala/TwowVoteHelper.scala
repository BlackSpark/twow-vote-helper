/**
  * Created by jonak on 6/21/2017.
  */

import scala.collection.mutable
import scalafx.Includes._
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.{JFXApp, Platform}
import scalafx.beans.binding.Bindings
import scalafx.beans.property.{BooleanProperty, StringProperty}
import scalafx.event.ActionEvent
import scalafx.scene.Scene
import scalafx.scene.control.{Button, TextArea}
import scalafx.scene.image.Image
import scalafx.scene.input.{Clipboard, ClipboardContent}
import scalafx.scene.layout.{ColumnConstraints, GridPane, RowConstraints}
import scalafx.stage.WindowEvent

object TwowVoteHelper extends JFXApp
{
    /**
      * Gets the word count for a response.
      *
      * @param response The TWOW Response to count the words for.
      * @return The word count.
      */
    def getWordCount(response: String): Int =
    {
        val mat = "^.*\t(\\d{1,4})$".r.findFirstMatchIn(response)
        if (mat.isDefined)
            mat.head.group(1).toInt
        else
            "[^\\s]+(\\s?)+".r.findAllIn(response).length
    }
    
    /** True on first load, used to keep a spreadsheetEntry from taking focus on application start. **/
    val firstLoad = BooleanProperty(true)
    /** Keyword of the current screen. **/
    val screen = StringProperty("")
    /** Maps letters to responses. **/
    val voteScreenMap = mutable.Map[String, String]()
    
    
    stage = new PrimaryStage
    {
        title = "TWOW Vote Helper"
        
        
        // ===== Scene Elements and Grid Constraints =====
        val voteEntry = new TextArea
        {
            promptText = "Enter your vote (ABCDEF...) here"
            wrapText() = true
            editable() = true
        }
        GridPane.setConstraints(voteEntry, 1, 0, 1, 1)
        
        val spreadsheetEntry = new TextArea
        {
            promptText = "Paste from spreadsheet here"
            wrapText() = false
            editable() = true
        }
        GridPane.setConstraints(spreadsheetEntry, 0, 0, 1, 2)
        
        val voteStatus = new TextArea
        {
            promptText = "This area contains information about how you've voted on this screen."
            wrapText() = true
            editable() = false
        }
        GridPane.setConstraints(voteStatus, 2, 0, 1, 3)
        
        val voteOut = new TextArea
        {
            promptText = "Vote output is generated here. Press Copy to copy this field."
            editable() = false
            wrapText() = false
            
        }
        GridPane.setConstraints(voteOut, 1, 2, 1, 1)
        
        val copyButton = new Button
        {
            text() = "Copy"
            minWidth = 100
            prefWidth = 100
            maxWidth = 300
        }
        GridPane.setConstraints(copyButton, 1, 1, 1, 1)
        
        val clearButton = new Button
        {
            text() = "Clear"
            maxWidth = 100
        }
        GridPane.setConstraints(clearButton, 0, 2, 1, 1)
        
        
        // ===== Listeners =====
        /** Checks new vote entries for validity. **/
        voteEntry.text.onChange {
            (_, _, newVal) => {
                var check = Set[Char]()
                for (letter <- newVal) {
                    val cnt = newVal.count(_ == letter)
                    if (cnt > 1 || !voteScreenMap.keys.toList.contains(letter.toString)) {
                        check += letter
                    }
                }
                if (check.nonEmpty) {
                    var replaced = newVal.toUpperCase
                    for (letter <- check) {
                        replaced = replaced.replaceFirst(letter.toString, "")
                        //TODO: Instead of replacing the leftmost character with the rightmost, it should replace the old location with the new.
                    }
                    Platform.runLater(() => {
                        voteEntry.text() = replaced
                        voteEntry.positionCaret(voteEntry.text().length())
                    })
                }
            }
        }
        
        /** Gets the votes from the spreadsheet and also clears everything else **/
        spreadsheetEntry.text.onChange {
            (_, _, newVal) => {
                voteScreenMap.clear()
                try {
                    val entries = newVal.split("\n")
                    for (entry <- entries) {
                        val letter = entry.substring(0, 1)
                        val response = entry.substring(1).trim
                        if (letter.trim == "" || (!entry.contains(" ") && !entry.contains("\t"))) {
                            screen() = entry.trim
                        }
                        else {
                            if (voteScreenMap.contains(letter))
                                voteScreenMap(letter) = response
                            else
                                voteScreenMap += letter -> response
                        }
                    }
                }
                catch {
                    case _: UnsupportedOperationException => println("Can't find any entries...")
                    case _: StringIndexOutOfBoundsException => println("Looks like the vote entry is empty!")
                }
                voteEntry.text() = ""
            }
        }
        /** Prevents spreadsheetEntry from taking focus on application start. **/
        spreadsheetEntry.focused.addListener((_, _, _) => {
            if (firstLoad()) {
                pane.requestFocus()
                firstLoad() = false
            }
        })
        
        /** Copies voteOut.text() to clipboard on press. **/
        copyButton.onAction =
            (event: ActionEvent) => {
                val content = new ClipboardContent()
                content.putString(voteOut.text())
                Clipboard.systemClipboard.content = content
            }
        
        /** Clears spreadsheetEntry.text(), which in turn will clear everything else **/
        clearButton.onAction =
            (event: ActionEvent) => {
                Platform.runLater(() => {
                    spreadsheetEntry.text() = ""
                    spreadsheetEntry.requestFocus()
                })
            }
    
        // ===== Bindings =====
        /** Updates the vote status to show what's been voted on vs what's not, and show word counts. **/
        voteStatus.text <== Bindings.createStringBinding(
            () => {
                if (spreadsheetEntry.text() == "")
                    ""
                else {
                    val selected = new mutable.StringBuilder("")
                    val notSelected = new mutable.StringBuilder("")
                    for (ch <- voteEntry.text()) {
                        val k = ch.toString
                        if (voteScreenMap.contains(k)) {
                            val resp = voteScreenMap(k).replaceAll("\t(\\d{1,4})$", "")
                            selected ++= s"$k\t$resp\t${getWordCount(voteScreenMap(k))}\n"
                        }
                    }
                    for (k <- voteScreenMap.keys.toList.sorted) {
                        if (!voteEntry.text().contains(k)) {
                            val resp = voteScreenMap(k).replaceAll("\t(\\d{1,4})$", "")
                            notSelected ++= s"$k\t$resp\t${getWordCount(voteScreenMap(k))}\n"
                        }
                    }
                    selected.append("-----------\n")
                    selected.append(notSelected.toString)
                    selected.toString
                }
            },
            voteEntry.text
        )
        
        /** Updates the vote out text with the screen and the vote. **/
        voteOut.text <== Bindings.createStringBinding(
            () => {
                if (voteEntry.text() == "" || screen() == "")
                    ""
                else
                    s"[${screen()} ${voteEntry.text()}]"
            }, voteEntry.text
        )
        
        
        // ===== Row and Column Info =====
        val rowInfoSmall = new RowConstraints
        {
            prefHeight = 100
        }
        val rowInfoMedium = new RowConstraints
        {
            prefHeight = 100
        }
        val colInfoSmall = new ColumnConstraints
        {
            prefWidth = 100
            maxWidth = 100
        }
        val colInfoMedium = new ColumnConstraints
        {
            minWidth = 100
            prefWidth = 100
            maxWidth = 200
        }
        val colInfoBig = new ColumnConstraints
        {
            prefWidth = 100
        }
        
        
        // ===== Pane and Scene Setup =====
        val pane = new GridPane
        {
            rowConstraints.add(rowInfoSmall)
            columnConstraints.add(colInfoSmall)
            rowConstraints.add(rowInfoMedium)
            columnConstraints.add(colInfoMedium)
            rowConstraints.add(rowInfoSmall)
            columnConstraints.add(colInfoBig)
            
            children ++= Seq(spreadsheetEntry, voteEntry, voteOut, voteStatus, copyButton, clearButton)
        }
        
        scene = new Scene(750, 300)
        {
            content = pane
        }
        
        
        // ===== Height and Width Scaling =====
        rowInfoMedium.prefHeight <== scene().height * 0.6
        rowInfoSmall.prefHeight <== scene().height * 0.2
        colInfoMedium.prefWidth <== scene().width / 3
        colInfoSmall.prefWidth <== scene().width / 5
        colInfoBig.prefWidth <== scene().width - copyButton.width - spreadsheetEntry.width
        copyButton.prefHeight <== scene().height * 0.6
        copyButton.prefWidth <== scene().width / 3
        clearButton.prefHeight <== scene().height * 0.2
        clearButton.prefWidth <== scene().width / 5
    }
    
    
    // ===== Stage Setup =====
    stage.icons += new Image(getClass.getResourceAsStream("twow.png"))
    stage.onCloseRequest = (e: WindowEvent) => Platform.exit()
}